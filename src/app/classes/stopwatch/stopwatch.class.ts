export class Stopwatch {
  startFlag = <boolean>true;
  resetFlag = <boolean>false;

  totalTime = <number>0;
  startTime = <number>new Date().getTime();
  currentTime = <number>new Date().getTime();

  mainTime = <number>0;
  prevTime = <number>0;
  times: Times[] = [];
}

export class Times {
  constructor(private mainTime: number, private diffTime: number) {
    this.mainTime = mainTime;
    this.diffTime = diffTime;
  }
}
