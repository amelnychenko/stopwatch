import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {TimeDirective} from './directives';
import {StorageService} from './services';

@NgModule({
  declarations: [
    AppComponent,
    TimeDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [StorageService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
