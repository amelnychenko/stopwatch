import {Directive, ElementRef, Input, OnChanges} from '@angular/core';

@Directive({
  selector: '[appTime]'
})
export class TimeDirective implements OnChanges {

  @Input() appTime: number;

  constructor(private elRef: ElementRef) {
  }

  ngOnChanges(): void {
    const ms = Math.floor(this.appTime % 10);

    let m = String(Math.floor(this.appTime / 600));
    let s = String(Math.floor(this.appTime % 600 / 10));

    m = String(m).length === 1 ? `0${m}` : m;
    s = String(s).length === 1 ? `0${s}` : s;

    this.elRef.nativeElement.innerHTML = `${m}:${s}:${ms}`;
  }
}
