import {Injectable, OnDestroy} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable()
export class StorageService implements OnDestroy {

  private onSubject = new Subject();
  public changes = this.onSubject.asObservable();

  constructor() {
    this.start();
  }

  ngOnDestroy() {
    this.stop();
  }

  public store(key: string, data: any): void {
    localStorage.setItem(key, JSON.stringify(data));
    this.onSubject.next();
  }

  private start(): void {
    window.addEventListener('storage', this.storageEventListener.bind(this));
  }

  private stop(): void {
    window.removeEventListener('storage', this.storageEventListener.bind(this));
    this.onSubject.complete();
  }

  private storageEventListener(event: StorageEvent) {
    if (event.storageArea === localStorage) {
      this.onSubject.next();
    }
  }
}
