import {Component, OnDestroy, OnInit} from '@angular/core';

import {Stopwatch, Times} from './classes';
import {StorageService} from './services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  private subscribers: any[] = [];
  private STOPWATCH_KEY = 'stopwatch';
  private stopwatch: Stopwatch = new Stopwatch();
  private stopwatchInterval: any;

  constructor(private storageService: StorageService) {
  }

  ngOnInit(): void {
    this.onInit();
    this.subscribers.push(this.storageService.changes.subscribe(() => this.onInit()));
  }

  ngOnDestroy() {
    for (const item of this.subscribers) {
      item.unsubscribe();
    }
  }


  private onInit() {
    clearInterval(this.stopwatchInterval);

    this.stopwatch = JSON.parse(localStorage.getItem(this.STOPWATCH_KEY)) || new Stopwatch();

    if (!this.stopwatch.startFlag) {
      this.stopwatchInterval = setInterval(() => this.getTime(), 100);
    }
  }

  private onStart() {
    if (this.stopwatch.startFlag) {
      this.stopwatch.startFlag = false;
      this.stopwatch.resetFlag = true;

      this.stopwatch.totalTime = this.stopwatch.mainTime;
      this.stopwatch.startTime = new Date().getTime();

      this.stopwatchInterval = setInterval(() => this.getTime(), 100);
    } else {
      clearInterval(this.stopwatchInterval);

      this.stopwatch.startFlag = true;
    }

    this.storageService.store(this.STOPWATCH_KEY, this.stopwatch);
  }

  private onSplit() {
    if (!this.stopwatch.startFlag) {
      this.stopwatch.times.unshift(new Times(this.stopwatch.mainTime, this.stopwatch.mainTime - this.stopwatch.prevTime));

      this.stopwatch.prevTime = this.stopwatch.mainTime;

      this.storageService.store(this.STOPWATCH_KEY, this.stopwatch);
    }
  }

  private onReset() {
    if (this.stopwatch.resetFlag) {
      clearInterval(this.stopwatchInterval);

      this.stopwatch = new Stopwatch();

      this.storageService.store(this.STOPWATCH_KEY, this.stopwatch);
    }
  }

  private onDeleteItem(i) {
    this.stopwatch.times.splice(i, 1);

    this.storageService.store(this.STOPWATCH_KEY, this.stopwatch);
  }

  private getTime() {
    this.stopwatch.currentTime = new Date().getTime();
    this.stopwatch.mainTime = this.stopwatch.totalTime + Math.floor((this.stopwatch.currentTime - this.stopwatch.startTime) / 100);
  }
}
